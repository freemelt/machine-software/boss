# SPDX-FileCopyrightText: 2019,2020 Freemelt AB
#
# SPDX-License-Identifier: GPL-3.0-only

from setuptools import setup, find_packages
from boss import __version__

with open('README.md') as readme_file:
    long_description = readme_file.read()

setup(
    name='boss',
    version=__version__,
    maintainer='Freemelt AB',
    maintainer_email='3Drevolution@freemelt.com',
    description='Control software for metal 3D printers from Freemelt AB',
    long_description=long_description,
    url='https://gitlab.freemelt.com/openmelt/BuildOperationStaticSequencer',
    packages=find_packages(exclude=['tests']),
    install_requires=[
        'cryptography',
        'paho-mqtt>=1.4.0',
        'pyyaml>=5.1',
        'grpcio>=1.25.0',
        'grpcio-tools>=1.20.1',
        'protobuf>=3.9.1'
    ],
    python_requires='>=3.7',
    classifiers=[
        'Environment :: Console',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: CPython'
    ]
)
