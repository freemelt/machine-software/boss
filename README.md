<!--
SPDX-FileCopyrightText: 2019,2020 Freemelt AB

SPDX-License-Identifier: GPL-3.0-only
-->

# Build Operation Static Sequencer (BOSS)
BOSS is a build sequencer that runs a build with a static repeated sequence. It will initially heat the build start plate to a desired temperature and then coat it with  powder then heat and melt. This powder recoat, heat and melt sequence is then repeated throughout the build. The melt pattern can vary between the layers and the length of the heating sequence is easily changed during a build.


# License
Copyright © 2019, 2020 Freemelt AB <opensource@freemelt.com>

BOSS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.

The examples provided in the folder named "examples" are licensed as [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)