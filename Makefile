# SPDX-FileCopyrightText: 2019,2020 Freemelt AB
#
# SPDX-License-Identifier: GPL-3.0-only

PKGNAME = $(shell dpkg-parsechangelog -S source)
VERSION = $(shell dpkg-parsechangelog -S version | sed 's/-.*//')
DIST = $(shell dpkg-parsechangelog -S distribution)

all:

boss/__init__.py: boss/__init__.py.in
	sed -e "s,[@]VERSION[@],$(VERSION),g" < boss/__init__.py.in > boss/__init__.py

boss/freemeltapi/BossRpc_pb2.py: freemeltapi/protos/BossRpc.proto
	ln -s -r -f freemeltapi/protos/BossRpc.proto boss/freemeltapi/BossRpc.proto
	ln -s -r -f freemeltapi/protos/BeamServiceRpc.proto boss/freemeltapi/BeamServiceRpc.proto
	ln -s -r -f freemeltapi/protos/ElectronServiceRpc.proto boss/freemeltapi/ElectronServiceRpc.proto
	ln -s -r -f freemeltapi/protos/RecoaterServiceRpc.proto boss/freemeltapi/RecoaterServiceRpc.proto
	python3 -m grpc_tools.protoc -I=. --python_out=. --grpc_python_out=. boss/freemeltapi/*.proto
	ln -s -r -f OpenBeamPath/OBP.proto boss/OBP.proto
	python3 -m grpc_tools.protoc -I=. --python_out=. --grpc_python_out=. boss/OBP.proto

release: boss/__init__.py boss/freemeltapi/BossRpc_pb2.py
	tar --numeric-owner --group 0 --owner 0 -cJh \
	  --xform "s,^,$(PKGNAME)-$(VERSION)/," \
	  -f $(PKGNAME)-$(VERSION).tar.xz \
	  boss/*.py boss/freemeltapi/*.py tests/*.py setup.py boss.yaml boss/freemeltapi/*.proto \
	  README.md

deb-ci:
	if [ -z "$$CI_COMMIT_TAG" ]; then \
	  DEBFULLNAME="$$GITLAB_USER_NAME" DEBEMAIL="$$GITLAB_USER_EMAIL" \
	   dch -l~git.$$CI_PIPELINE_ID.$$CI_COMMIT_SHORT_SHA "Untagged build" -D unstable; \
	else \
	  if [ "$$CI_COMMIT_TAG" != "$(VERSION)" ] && [ "$$CI_COMMIT_TAG" != "v$(VERSION)" ]; then \
	     echo "debian/changelog has not been updated to the new version!"; \
	     exit 1; \
	  fi; \
	  if [ "$(DIST)" != "stable" ]; then \
	     echo "debian/changelog must use stable for tagged releases (DIST=$$DIST)!"; \
	     exit 1; \
	  fi \
	fi

deb: release
	rm -rf $(PKGNAME)-$(VERSION)
	tar -xJf $(PKGNAME)-$(VERSION).tar.xz
	ln -sf $(PKGNAME)-$(VERSION).tar.xz \
	  $(PKGNAME)_$(VERSION).orig.tar.xz
	cp -a debian/ $(PKGNAME)-$(VERSION)/
	(cd $(PKGNAME)-$(VERSION) && DEB_BUILD_OPTIONS=noddebs dpkg-buildpackage -us -uc -b)

clean:
	-rm -rf freemelt-boss[-_]*