# SPDX-FileCopyrightText: 2019,2020 Freemelt AB
#
# SPDX-License-Identifier: Apache-2.0

import grpc
import BossRpc_pb2
import BossRpc_pb2_grpc
channel = grpc.insecure_channel('0.0.0.0:31403')
stub = BossRpc_pb2_grpc.BossRPCServiceStub(channel)
# Load build file
stub.LoadBuild(BossRpc_pb2.LoadBuildFileCommand(buildFilename="/var/freemelt/build.yaml"))
# Start build
stub.BuildProcessControl(BossRpc_pb2.BuildCommand(command="start")) # start/stop/pause
# Update pre-heat file/repetitions
stub.BuildProcessPreheat(BossRpc_pb2.PreheatCommand(preheatFilename="/var/freemelt/file.obp",repetitions=5))
