# SPDX-FileCopyrightText: 2019,2020 Freemelt AB
#
# SPDX-License-Identifier: GPL-3.0-only



# Validates different files
# Built-in
import sys
import os
import logging

# PyPI
import yaml
from google.protobuf.internal.decoder import _DecodeVarint32

# Freemelt
from . import OBP_pb2 as OBP

log = logging.getLogger("validate")


class ParseError(Exception):
    """Could not parse file"""


def build_file(filename): # TBD: Pass file type as argument?
    # Parses file as YAML
    # See if all required arguments exist
    # Validate arguments; files exists, values within bounds etc

    var_exists = [
        "build|start_heat|file",
        "build|start_heat|timeout",
        "build|start_heat|temp_sensor",
        "build|start_heat|target_temperature",
        "build|preheat|file",
        "build|preheat|repetitions",
        "build|build|layers",
        "build|build|files",
        "build|layerfeed|build_piston_distance",
        "build|layerfeed|powder_piston_distance",
        "build|layerfeed|recoater_advance_speed",
        "build|layerfeed|recoater_retract_speed",
        "build|layerfeed|recoater_dwell_time",
        "build|layerfeed|recoater_full_repeats",
        "build|layerfeed|recoater_build_repeats",
        "build|layerfeed|triggered_start"
    ]

    files_path = [
        "build|start_heat|file",
        "build|preheat|file",
    ]

    pattern_files_path = "build|build|files"

    with open(filename, 'r') as fp:
        data = None
        try:
            data = yaml.safe_load(fp)
            log.debug(data)
        except yaml.YAMLError as exc:  # Not a yaml file
            log.debug(exc)
            raise

        # Check if required parameters in build file exist
        for param in var_exists:
            log.info(f"Checking param {param}..")
            if _parameter(data, param) is None:
                raise ParseError(f"Parameter {param} missing from file {filename}")

        # Check if required files in build file exist
        for file in files_path:
            f = _parameter(data, file)
            log.info(f"Checking file {f}..")
            if os.path.exists(f) == False:
                raise FileNotFoundError(f"File {f} not found on disk")

        # Validate file contents
        for file in files_path:
            f = _parameter(data, file)
            log.info(f"Validating {f}..")

            if obp_file(f) == False:
                raise ParseError(f"'{f}' is not a proper OBP-file")

        # Validate all pattern files
        # Check if required files in build file exist
        pattern_files = _parameter(data, pattern_files_path)
        for file in pattern_files:
            log.info(f"Checking file {f}..")
            if os.path.exists(f) == False:
                raise FileNotFoundError(f"File {f} not found on disk")

        # Validate file contents
        for file in pattern_files:
            log.info(f"Validating {f}..")

            if obp_file(f) == False:
                raise ParseError(f"'{f}' is not a proper OBP-file")

        # TODO: Validate layerfeed parameters

def _parameter(data_dict, path): # |-delimited path hierarchy
    point = data_dict
    log.debug(f"Looking for {path.split('|')}")
    for arg in path.split("|"):
        try:
            point = point[arg] # Dig deeper
        except Exception as e:
            log.debug(f"Argument {arg} not in dict {point}: Error: {e}")
            return None
    return point

def obp_file(filename):
    """Validates if the OBP file is a proper OBP file"""
    with open(filename, 'rb') as f:
        payload = f.read()
        p = OBP.Packet()
        value, new_pos = _DecodeVarint32(payload, 0)
        # Do a basic check for size
        # TODO: 1024 is randomly chosen, choose better value
        if value < 0 or value > 1024:
            raise ParseError(f"Size wrong {value} > 1024. Indicates a bad OBP-file.")
        try:
            p.ParseFromString(payload[new_pos:new_pos + value])
        except Exception as e:
            raise ParseError(f"Could not parse {filename}: {e}")
