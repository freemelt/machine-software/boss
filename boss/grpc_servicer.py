# SPDX-FileCopyrightText: 2019,2020 Freemelt AB
#
# SPDX-License-Identifier: GPL-3.0-only

# Built-in imports
import enum
import logging
import threading
import time
import queue

# PyPI imports
import grpc
import yaml

# Freemelt projects
from servicelib.mqtt import MQTTMessage, MQTTAttribute

# Project imports
from . import validate
from .freemeltapi import (
    # gRPC generated
    BossRpc_pb2,
    BossRpc_pb2_grpc,
    BeamServiceRpc_pb2,
    BeamServiceRpc_pb2_grpc,
    ElectronServiceRpc_pb2,
    ElectronServiceRpc_pb2_grpc,
    RecoaterServiceRpc_pb2,
    RecoaterServiceRpc_pb2_grpc
)


LOG = logging.getLogger(__name__)
BEAM_STUB = None
RECOATER_STUB = None
ELECTRON_STUB = None

#
# BOSS in Influx, TBD if we tag each layer with the filename, or each build state with the build file
# Measurements    |  Fields          | Tags  | Comment
# --------------------------------------------------------------------------------------
# "Boss"          |  "State"         |       | State: 0-STOPPED, 1-STARTED, 2-HEATING, 3-BUILDING, 4-PAUSED, 5-COMPLETED
# "Boss"          |  "BuildLayers"   |       | Number of layers in a loaded build
# "Boss"          |  "CurrentLayer"  |       | Current Layer
# "Boss"          |  "Layerfeed"     |       | 1 = Feeding, 0 = Not feeding

class Build(enum.Enum):
    """Signal changes requested by the user from gRPC"""
    STOPPED = 0
    STARTED = 1
    PAUSED = 4


class BuildState:
    """Keeps the current state of BOSS

    The `generator_*` attributes are loaded from the boss configuration.
    Most of the other attributes are loaded from a build file.
    """

    # These attributes are automatically refreshed by mqtt.
    # Keep in mind that you will get the default value if the service
    # is not running (e.i. publishes alive=False).
    hv_state = MQTTAttribute(
        topic='freemelt/0/ElectronService/0/HighVoltageGenerator/State/Enter',
        field='state',
        default='N/A'
    )
    beam_state = MQTTAttribute(
        topic='freemelt/0/ElectronService/0/Beam/State/Enter',
        field='state',
        default='N/A'
    )
    pattern_active_status = MQTTAttribute(
        topic='freemelt/0/ElectronService/0/Beam/PatternActive/Status',
        field='status',
        default=True
    )
    hv_current_actual = MQTTAttribute(
        topic='freemelt/0/ElectronService/0/HighVoltageGenerator/Type/Current',
        field='CurrentActual',
        default=float('nan')
    )
    build_temperature_sensor1 = MQTTAttribute(
        topic='freemelt/0/ChamberService/0/BuildTemperature/Name/Sensor1',
        field='Temperature',
        default=float('nan')
    )
    build_temperature_sensor2 = MQTTAttribute(
        topic='freemelt/0/ChamberService/0/BuildTemperature/Name/Sensor2',
        field='Temperature',
        default=float('nan')
    )
    build_temperature_sensor3 = MQTTAttribute(
        topic='freemelt/0/ChamberService/0/BuildTemperature/Name/Sensor3',
        field='Temperature',
        default=float('nan')
    )
    build_temperature_sensor4 = MQTTAttribute(
        topic='freemelt/0/ChamberService/0/BuildTemperature/Name/Sensor4',
        field='Temperature',
        default=float('nan')
    )

    def __init__(self):
        self.status = Build.STOPPED

        # From build file/gRPC commands
        self.build_file = None

        ## Start heat
        self.start_heat_file = None
        self.start_heat_sensor = ""
        self.start_heat_target_temperature = 0.0
        self.start_heat_timeout = 0

        ## Pre heat between layers
        self.preheat_file = None
        self.preheat_repetitions = 0

        ## Patterns to draw
        self.patterns_files = []
        self.patterns_layers = 0
        self.current_layer = 0

        ## Layer feed params
        self.build_piston_distance = 0.0
        self.powder_piston_distance = 0.0
        self.recoater_advance_speed = 0.0
        self.recoater_retract_speed = 0.0
        self.recoater_dwell_time = 0.0
        self.recoater_full_repeats = 0
        self.recoater_build_repeats = 0
        self.triggered_start = False

    def load_build_file(self, filename):
        """Build files are loaded from gRPC"""
        with open(filename, 'r') as fp:
            try:
                data = yaml.safe_load(fp)
            except yaml.YAMLError as exc:  # Not a yaml file
                logging.error(str(exc))
                raise

        # Setup the build state
        self.build_file = filename

        # Start heat
        self.start_heat_file = data["build"]["start_heat"]["file"]
        self.start_heat_target_temperature = data["build"]["start_heat"]["target_temperature"]
        self.start_heat_sensor = data["build"]["start_heat"].get("temp_sensor", "Sensor1")     # Default Sensor1
        self.start_heat_timeout = data["build"]["start_heat"].get("timeout", 14400)            # Default 14400

        # Pre heat
        self.preheat_file = data["build"]["preheat"]["file"]
        self.preheat_repetitions = max(1, data["build"]["preheat"]["repetitions"])

        ## Layerfeed w/ default values
        self.build_piston_distance = data["build"].get("layerfeed", dict()).get("build_piston_distance", -0.1)
        self.powder_piston_distance = data["build"].get("layerfeed", dict()).get("powder_piston_distance", 0.2)
        self.recoater_advance_speed = data["build"].get("layerfeed", dict()).get("recoater_advance_speed", 200.0)
        self.recoater_retract_speed = data["build"].get("layerfeed", dict()).get("recoater_retract_speed", 200.0)
        self.recoater_dwell_time = data["build"].get("layerfeed", dict()).get("recoater_dwell_time", 500)
        self.recoater_full_repeats = data["build"].get("layerfeed", dict()).get("recoater_full_repeats", 0)
        self.recoater_build_repeats = data["build"].get("layerfeed", dict()).get("recoater_build_repeats", 0)
        self.triggered_start = data["build"].get("layerfeed", dict()).get("triggered_start", True)

        # Total number of layers (int)
        self.patterns_layers = data["build"]["build"]["layers"]

        # Pattern files - a list of filenames
        self.patterns_files = list(data["build"]["build"]["files"])


def start_HV(cfg):
    """Use Electrion service RPC to start Hight Voltage generator.

    Return True on success, False on failure.
    """
    # Shortcut:
    short_cfg = cfg["Service:ElectronRPCService:GeneratorParams"]
    cmd = ElectronServiceRpc_pb2.GeneratorControlCommand(
        start=True,
        beam_disabled=False,
        target_voltage=short_cfg.get_float("TargetVoltage", 60000.0),
        ramp_time=short_cfg.get_float("RampTime", 5000.0),
        steps=short_cfg.get_int("Steps", 10),
        ramp_mode=short_cfg.get_int("RampMode", 0)
    )
    logging.debug('RPC to Electron Service:\n%r', cmd)
    try:
        ELECTRON_STUB.GeneratorControl(cmd)
    except grpc.RpcError as error:
        LOG.error('Failed when starting HV: %r', error)
        return False
    return True


def stop_HV() -> bool:
    """Use Electrion service RPC to stop Hight Voltage generator.

    Return True on success, False on failure.
    """
    cmd = ElectronServiceRpc_pb2.GeneratorControlCommand(
        start=False)
    logging.debug('RPC to Electron Service:\n%r', cmd)
    try:
        ELECTRON_STUB.GeneratorControl(cmd)
    except grpc.RpcError as error:
        LOG.error('Failed when stopping HV: %r', error)
        return False
    return True


class State:
    """Very basic state all states should inherit.

    The state-machine loop should continouosly call the `run` method
    until the state is changed. When the state change the current
    state's `exit` method should be called and the new state's
    `enter` method should be called. Each state instance should be
    created before the state-machine loop starts (e.i. `__init__` is
    never called again during the loop lifetime).

    The `run` method is responsible for returning the next state
    instance.
    """

    def __init__(self, cfg, mqtt_client):
        log_name = f'BossMAIN:{self.__class__.__name__}'
        self.log = logging.getLogger(log_name)
        self.cfg = cfg
        self.mqtt = mqtt_client
        self.topic_base = cfg['Service:MQTT:TopicBase']

    def __str__(self):
        return self.__class__.__name__

    def enter(self, build_state):
        # State-machine states
        topic = f'{self.topic_base}/Boss/State/Boss'
        m = MQTTMessage(topic, {
            'BossState': self.__class__.__name__,
            'BuildStatus': build_state.status.name,
            'BuildStatusNumber': build_state.status.value})
        self.mqtt.publish(m.topic, m.payload, retain=True)

        # Build progress
        topic = f'{self.topic_base}/Boss/Layers/Progress'
        m = MQTTMessage(topic, {
            'BuildLayers': build_state.patterns_layers,
            'CurrentLayer': build_state.current_layer})
        self.mqtt.publish(m.topic, m.payload, retain=True)

    def exit(self, build_state):
        pass

    def run(self, build_state):
        return self


class IdleState(State):
    """Idle State - Do nothing until a build is started.

    When a build is started, then goto the Start HV State (which will
    be followed by Start Heat State).
    """

    def __init__(self, cfg, mqtt_client, start_hv_state, start_heat_state):
        super().__init__(cfg, mqtt_client)
        self.start_hv_state = start_hv_state
        self.start_heat_state = start_heat_state

    def enter(self, build_state):
        build_state.current_layer = 0
        cmd = BeamServiceRpc_pb2.StartExposureRequest(clear_queue=True)
        BEAM_STUB.StartExposure(cmd)
        super().enter(build_state)

    def run(self, build_state):
        if build_state.status != Build.STARTED:
            time.sleep(1)
            return self
        # We got a start command

        # Set where the HV state should go after success
        self.start_hv_state.next_state = self.start_heat_state
        return self.start_hv_state


class StartHVState(State):
    """Start HV State - Try to start HV generator and get a stable beam.

    The goal is to start the HV generator and get a stable beam. When
    trying to do this we may run into two different problems:

    * The HV generator failes to start.
    * The HV generator starts BUT the beam does not get stable within
      the `BEAM_STABLE_TIMEOUT` timeout.

    If any of these problems occurs, we try again until we reach the
    maximum number of allowed retires: `MAX_RETRIES`. When that
    happens goto the *Stop State*.

    On success, the next state is decided by the `next_state`
    attribute. It is set to *Stop State* by default, but should
    be set to the *Start Heat State* in normal circumstances.

    This state uses the Electron Service gRPC methods to interact
    with with the HV generator. And uses a MQTT subscription to get
    information about the beam.
    """
    MAX_RETRIES = 5
    BEAM_ACTIVE_TIMEOUT = 360  # Seconds

    def __init__(self, cfg, mqtt_client, stop_state):
        super().__init__(cfg, mqtt_client)
        self.retry_timeout = 1.0  # Seconds
        self.retries = 0
        self.stop_state = stop_state
        self.next_state = self.stop_state

    def run(self, build_state):

        while self.retries < self.MAX_RETRIES:
            # Start high voltage generator
            if start_HV(self.cfg):
                break  # Success
            elif build_state.status == Build.STOPPED:
                # Stop from gRPC requested
                return self.stop_state
            else:
                self.retries += 1
                self.log.warning("Unable to start HV, attempt (%d/%d)",
                                 self.retries, self.MAX_RETRIES)
                time.sleep(self.retry_timeout)
        else:
            # Tried too many times? Goto stopped state.
            self.log.error("Failed to start HV generator after %d attempts.",
                           self.retries)
            return self.stop_state
        self.log.info('The HV generator state: %s', build_state.hv_state)

        # Wait for stable beam.
        t0 = time.monotonic()
        deadline = t0 + self.BEAM_ACTIVE_TIMEOUT
        while build_state.beam_state != 'active':
            t1 = time.monotonic()
            self.log.info(
                'Current beam state: %s. Time left: %.2f seconds. Timeout for '
                'beam active is set to %.2f seconds.', build_state.beam_state,
                deadline - t1, self.BEAM_ACTIVE_TIMEOUT)

            # Timeout?
            if t1 >= deadline:
                self.log.warning("Timeout! Beam not active after %.2f seconds",
                                 self.BEAM_ACTIVE_TIMEOUT)
                return self.stop_state

            # HV arc trip?
            if build_state.hv_state in {'error', 'generator_off'}:
                self.log.warning('HV Generator state: %r', build_state.hv_state)
                return self.stop_state

            # Someone stopped through gRPC?
            if build_state.status == Build.STOPPED:
                return self.stop_state

            time.sleep(3)

        self.log.info("Beam stable - Proceed")
        return self.next_state

    def exit(self, build_state):
        # Reset `retries` counter when we leave the state
        self.retries = 0


class StopState(State):
    """Stop State - Stop HV generator and goto *Idle State*."""

    def __init__(self, cfg, mqtt_client, idle_state):
        super().__init__(cfg, mqtt_client)
        self.idle_state = idle_state

    def enter(self, build_state):
        build_state.status = Build.STOPPED
        super().enter(build_state)

    def run(self, build_state):
        # Stop HV when reaching stop state - Regardless of reason
        stop_HV() # TODO: Check if success
        return self.idle_state


class StartHeatState(State):
    """Start Heat State - Instruct BeamService to heat the build plate.
    """

    def __init__(self, cfg, mqtt_client, layer_feed_state, stop_state):
        super().__init__(cfg, mqtt_client)
        self.layer_feed_state = layer_feed_state
        self.stop_state = stop_state

        self.clear_queue_event = threading.Event()
        self.grpc_done = threading.Event()
        self.temperature_reached = False

    def enter(self, build_state):
        super().enter(build_state)
        self.clear_queue_event.clear()
        self.grpc_done.clear()
        self.monitor_thread = threading.Thread(
            target=self.monitor_temperature,
            args=(build_state,),
            name='ThreadMonitorTemp',
            daemon=True)
        self.log.info(
            "Heating plate with file: %s", build_state.start_heat_file)
        self.log.info(
            "Timeout set to: %d seconds", build_state.start_heat_timeout)
        self.log.info(
            "Using heat sensor: %s", build_state.start_heat_sensor)
        self.log.info(
            "Target temperature: %.2f C", build_state.start_heat_target_temperature)

    def exit(self, build_state):
        # We normally exit to the Layerfeed state = Building has begun
        # Build status building
        self.clear_queue_event.set()
        self.grpc_done.set()

    def beam_requests(self, build_state):
        """Instruct Beam service to heat the plate."""
        self.log.info('Requesting Beam service to start exposure ...')
        yield BeamServiceRpc_pb2.Request(
            id=1,
            obp_file=build_state.start_heat_file,
            # Very high number of repetitions => Heat forever.
            # We tell it to stop later by clearing the queue.
            repetitions=4294967295,
            start_exposure=True
        )
        self.clear_queue_event.wait()
        self.log.info('Requesting Beam service to clear queue.')
        yield BeamServiceRpc_pb2.Request(
            id=2,
            clear_queue=True
        )
        self.grpc_done.wait()

    def monitor_temperature(self, build_state):
        """Continuously monitory the sensor for target temperature"""
        self.log.info('Build temperature monitoring thread started.')
        deadline = time.monotonic() + build_state.start_heat_timeout
        self.temperature_reached = False
        while not self.clear_queue_event.is_set():
            curr_temp = getattr(
                build_state,
                f'build_temperature_{build_state.start_heat_sensor.lower()}',
                float('nan'))
            self.log.info('Build temperature %s: %.2f C. Target: %.2f C. '
                          'Time left: %.1f sec', build_state.start_heat_sensor,
                          curr_temp, build_state.start_heat_target_temperature,
                          deadline - time.monotonic())
            if curr_temp >= build_state.start_heat_target_temperature:
                self.log.info('Target temperature reached.')
                self.temperature_reached = True
                self.clear_queue_event.set()
            elif time.monotonic() >= deadline:
                self.log.info('Timeout! Target temperature not reached.')
                self.clear_queue_event.set()
            elif build_state.status == Build.STOPPED:
                self.log.info('Stop was requested.')
                self.clear_queue_event.set()
            self.clear_queue_event.wait(3)
        self.log.info('Build temperature monitoring thread completed.')

    def run(self, build_state):
        if build_state.status == Build.STOPPED:
            return self.stop_state

        enum = BeamServiceRpc_pb2.Status.ExposureStatus  # Abbreviation
        for status in BEAM_STUB.OpenStream(self.beam_requests(build_state)):
            if status.status == enum.Value('ERRORED'):
                self.log.error('Beam service errored. Id=%d', status.status)
                return self.stop_state

            if status.id == 1:  # Start exposure request response
                if status.status == enum.Value('RECEIVED'):
                    self.log.info('Request to start exposure was received by '
                                  'Beam service.')
                    self.monitor_thread.start()
                    continue
                elif status.status == enum.Value('STOPPED'):
                    self.log.info('Heating stopped.')
                    self.clear_queue_event.set()
                    continue
                elif status.status == enum.Value('COMPLETED'):
                    self.log.error(
                        'Heating-forever completed (should be stopped by '
                        'clearing the queue).')
                    return self.stop_state

            elif status.id == 2:  # Clear queue request response
                if status.status == enum.Value('RECEIVED'):
                    self.log.info('Request to clear queue was received by '
                                  'Beam service.')
                    break

            self.log.error(
                'Unexpected status: %r. Id: %d.', status.status, status.id)
            return self.stop_state

        if build_state.status == Build.STOPPED:
            return self.stop_state
        elif self.temperature_reached:
            return self.layer_feed_state
        else:
            return self.stop_state


class LayerfeedState(State):
    """Layerfeed State - Do a layerfeed and goto *Pattern State* if OK.

    This state will call the RecoaterService gRPC method to perform a
    layerfeed cycle. On failure, goto the *Stop State*.

    After the layerfeed we verify that the beam is still stable. If not,
    goto the *Start HV State*. If stable, goto to the *Pattern State*.

    This state publishes a boolean indicating that a layerfeed is in
    action to the MQTT broker.
    """

    def __init__(self, cfg, mqtt_client, start_hv_state, pattern_state, stop_state):
        super().__init__(cfg, mqtt_client)
        self.start_hv_state = start_hv_state
        self.pattern_state = pattern_state
        self.stop_state = stop_state

    def enter(self, build_state):
        super().enter(build_state)
        topic = f'{self.topic_base}/Boss/Layers/Layerfeed'
        m = MQTTMessage(topic, {'Layerfeed:': 1})
        self.mqtt.publish(m.topic, m.payload)

    def exit(self, build_state):
        topic = f'{self.topic_base}/Boss/Layers/Layerfeed'
        m = MQTTMessage(topic, {'Layerfeed:': 0})
        self.mqtt.publish(m.topic, m.payload)

    def run(self, build_state):
        # Are we still suppose to run?
        if build_state.status == Build.STOPPED:
            return self.stop_state

        cmd = RecoaterServiceRpc_pb2.RecoatCycleCommand(
            basic_recoat_cycle=RecoaterServiceRpc_pb2.BasicRecoatCycle(
                build_piston_distance  = build_state.build_piston_distance,
                powder_piston_distance = build_state.powder_piston_distance,
                recoater_advance_speed = build_state.recoater_advance_speed,
                recoater_retract_speed = build_state.recoater_retract_speed,
                recoater_dwell_time    = build_state.recoater_dwell_time,
                recoater_full_repeats  = build_state.recoater_full_repeats,
                recoater_build_repeats = build_state.recoater_build_repeats,
                triggered_start        = build_state.triggered_start
            )
        )
        try:
            RECOATER_STUB.RecoatCycleControl(cmd)
        except grpc.RpcError as error:
            # Recoater probably timed out
            self.log.critical("Unable to complete Layerfeed. %r.", error)
            return self.stop_state

        # Check if beam is active
        if build_state.beam_state != 'active':
            self.log.warning(
                'Beam state not active (%s). Will restart HV and goto pattern '
                'state.', build_state.beam_state)
            # Set where the HV state should go after success
            self.start_hv_state.next_state = self.pattern_state
            return self.start_hv_state  # Try to restart the HV

        # We're good, move to next layer
        return self.pattern_state


class PatternState(State):
    """Pattern State - Make BeamService execute the next pattern files.

    This state sends the next OBP file (and preheat OBP file) to Beam-
    Service for execution. If success, goto the *Layerfeed State*.

    Note: The BeamService gRPC call is not blocking. Instead, it is
          RecoaterService that will block until the pattern is finished.

    When all layers are finished, (`current_layer >= pattern_layers`),
    the next state will be set to the *Complete State*.
    """

    def __init__(self, cfg, mqtt_client, layer_feed_state, stop_state, complete_state):
        super().__init__(cfg, mqtt_client)
        self.layer_feed_state = layer_feed_state
        self.stop_state = stop_state
        self.complete_state = complete_state
        self.grpc_done = threading.Event()

    def enter(self, build_state):
        self.grpc_done.clear()
        build_state.current_layer += 1  # Increase the layer count
        super().enter(build_state)

    def exit(self, build_state):
        self.grpc_done.set()

    def beam_requests(self, build_state):
        """Instruct Beam service to heat the plate."""
        self.log.info('Requesting Beam service to start preheat.')
        self.log.info('Preheat file: %s, repetitions: %d.',
                      build_state.preheat_file,
                      build_state.preheat_repetitions)
        yield BeamServiceRpc_pb2.Request(
            id=1,
            obp_file=build_state.preheat_file,
            # NOTE: Repetitions=0 implies that we will not get any
            #       COMPLETED/STOPPED/etc response from beam service.
            #       This will currently make the `run` method hang.
            repetitions=max(1, build_state.preheat_repetitions),
            start_exposure=True
        )
        self.log.info('Sending Beam service a build pattern to run when '
                      'preheat has finished.')
        index = build_state.current_layer % len(build_state.patterns_files)
        index -= 1  # Compensate increment at `enter`
        obp_file = build_state.patterns_files[index]
        self.log.info(
            'Layer: %d/%d, pattern file: %s', build_state.current_layer,
            build_state.patterns_layers, obp_file)
        yield BeamServiceRpc_pb2.Request(
            id=2,
            obp_file=obp_file,
            repetitions=1,
            start_exposure=True
        )
        self.grpc_done.wait()

    def run(self, build_state):
        if build_state.status == Build.STOPPED:
            return self.stop_state

        enum = BeamServiceRpc_pb2.Status.ExposureStatus  # Abbreviation
        for status in BEAM_STUB.OpenStream(self.beam_requests(build_state)):
            if status.status == enum.Value('ERRORED'):
                self.log.error('Beam service errored. Id=%d', status.status)
                return self.stop_state

            if status.id == 1:  # Start preheat request response
                if status.status == enum.Value('RECEIVED'):
                    self.log.info('Request to start preheat was received by '
                                  'Beam service.')
                    continue
                elif status.status == enum.Value('COMPLETED'):
                    self.log.info('Preheat completed.')
                    continue
                elif status.status == enum.Value('STOPPED'):
                    self.log.error('Preheat stopped.')
                    return self.stop_state

            elif status.id == 2:  # Start pattern request response
                if status.status == enum.Value('RECEIVED'):
                    self.log.info('Request to start pattern was received by '
                                  'Beam service.')
                    continue
                elif status.status == enum.Value('COMPLETED'):
                    self.log.info('Pattern completed.')
                    break
                elif status.status == enum.Value('STOPPED'):
                    self.log.error('Pattern stopped.')
                    return self.stop_state

            self.log.error(
                'Unexpected status: %r. Id: %d.', status.status, status.id)
            return self.stop_state

        # Check if we are done
        if build_state.current_layer >= build_state.patterns_layers:
            return self.complete_state
        return self.layer_feed_state  # Transition to next state


class CompleteState(State):
    """Complete State - The build is done. Sleep and goto *Stop State*.

    This state is entered after the *Pattern State* when all layers
    have been completed.

    In this state we sleep `hold_time` seconds before we enter the
    next state: *Stop State*. The sleep is done in order to make sure
    the BeamService has completed 'writing' the final pattern.
    """

    def __init__(self, cfg, mqtt_client, stop_state):
        super().__init__(cfg, mqtt_client)
        self.stop_state = stop_state

    def enter(self, build_state):
        super().enter(build_state)

    def run(self, build_state):
        time.sleep(3)
        if build_state.status == Build.STOPPED:
            # Stop requested through gRPC
            return self.stop_state

        hold_time = self.cfg.get_float("Criteria:CompletionHoldTime", 30.0)
        self.log.info(f"Hold {hold_time:0.2f} seconds until the last pattern is written.. ")

        deadline = time.monotonic() + hold_time

        # Wait until pattern active is LOW/FALSE
        while build_state.pattern_active_status:
            time.sleep(0.05)
            if time.monotonic() >= deadline:
                LOG.critical("PatternActiveStatus condition timed out")
                return self.stop_state
        LOG.info('PatternActiveStatus condition was successful.')

        # Wait for low beam current
        current_limit = self.cfg.get_float("Criteria:BeamPowerLow")
        while build_state.hv_current_actual > current_limit:
            time.sleep(0.05) # Poll time, 20Hz
            if time.monotonic() >= deadline:
                LOG.critical("BeamPowerLow condition timed out")
                return self.stop_state
        LOG.info('BeamPowerLow condition was successful.')

        return self.stop_state


def on_message(client, userdata, message):
    for inst in MQTTAttribute.instances:
        inst.update(message)


def on_connect(client, userdata, flags, rc):
    for topic in MQTTAttribute.topics():
        client.subscribe(topic)


class Servicer(BossRpc_pb2_grpc.BossRPCServiceServicer):

    def __init__(self, cfg, mqtt_client):
        super().__init__()
        self.mqtt = mqtt_client
        self.cfg = cfg

        self.build_state = BuildState()
        self.mqtt.user_data_set((cfg, self.build_state))
        self.mqtt.on_message = on_message
        self.mqtt.on_connect = on_connect

        self.log = logging.getLogger("BossRPC")
        self.state_machine = BossStateMachine(
            cfg, mqtt_client, self.build_state)

        global BEAM_STUB, RECOATER_STUB, ELECTRON_STUB
        # Setup Beam RPC client
        ip = self.cfg.get_str("Service:BeamRPCService:IP")
        port = self.cfg.get_str("Service:BeamRPCService:Port")
        channel = grpc.insecure_channel(f"{ip}:{port}")
        BEAM_STUB = BeamServiceRpc_pb2_grpc.BeamServiceControlStub(channel)

        # Setup Recoater RPC client
        ip = self.cfg.get_str("Service:RecoaterRPCService:IP")
        port = self.cfg.get_str("Service:RecoaterRPCService:Port")
        channel = grpc.insecure_channel(f"{ip}:{port}")
        RECOATER_STUB = RecoaterServiceRpc_pb2_grpc.RecoaterServiceStub(channel)

        # Setup Electron RPC client
        ip = self.cfg.get_str("Service:ElectronRPCService:IP")
        port = self.cfg.get_str("Service:ElectronRPCService:Port")
        channel = grpc.insecure_channel(f"{ip}:{port}")
        ELECTRON_STUB = ElectronServiceRpc_pb2_grpc.ElectronServiceControlStub(channel)

    def __enter__(self):
        on_connect(self.mqtt, (self.cfg, self.build_state), None, None)
        self.state_machine.start()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.log.info('Stopping service threads ...')
        self.state_machine.stop()
        self.state_machine.join()
        self.log.info('Stopped service threads.')

    def BuildProcessPreheat(self, request, context):
        self.log.info('BuildProcessPreheat called:\n%r', request)

        # Take file in request, validate, and transmit to BeamService
        file = request.preheat_filename

        # Did we receive a new preheat file?
        # If so, validate and store it
        if file:
            try:
                validate.obp_file(file)
            except FileNotFoundError as fe:
                self.log.error(str(fe))
                context.set_code(grpc.StatusCode.NOT_FOUND)
                context.set_details(file)
                return BossRpc_pb2.CommandStatus()
            except validate.ParseError as pe:
                self.log.error(str(pe))
                context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
                context.set_details(str(pe))
                return BossRpc_pb2.CommandStatus()
            # Load a new preheat file
            self.build_state.preheat_file = file

        # Always load the repetitions
        self.build_state.preheat_repetitions = max(1, request.repetitions)
        return BossRpc_pb2.CommandStatus()

    def LoadBuild(self, request, context):
        self.log.info('LoadBuild called:\n%r', request)
        # Take file in request, validate, and transmit to BeamService

        if self.build_state.status != Build.STOPPED:
            err_msg = 'Can only load a new build file while in state STOPPED'
            self.log.warning(err_msg)
            context.set_code(grpc.StatusCode.FAILED_PRECONDITION)
            context.set_details(err_msg)
            return BossRpc_pb2.CommandStatus()

        file = request.build_filename
        self.build_state.build_file = None
        try:
            validate.build_file(file)
        except FileNotFoundError as fe:
            self.log.error(str(fe))
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(file or 'Selected file')
            return BossRpc_pb2.CommandStatus()
        except validate.ParseError as pe:
            self.log.error(str(pe))
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(pe))
            return BossRpc_pb2.CommandStatus()

        # Load all variables from the build file into the build state
        self.build_state.load_build_file(file)
        return BossRpc_pb2.CommandStatus()

    def BuildProcessControl(self, request, context):
        """Changed state of the build; start, stop, pause.

        STOP: Only allowed in `started` state.
        PAUSE: Only allowed in `started` state.
        START: Only allowed in `stopped` or `paused` state.
        """
        self.log.info('BuildProcessControl called:\n%r', request)

        if request.command == 'start':
            if self.build_state.status in {Build.STOPPED, Build.PAUSED}:
                if self.build_state.build_file is None:
                    self.log.error('No valid build file loaded.')
                    context.set_code(grpc.StatusCode.FAILED_PRECONDITION)
                    context.set_details('No valid build file loaded.')
                    return BossRpc_pb2.CommandStatus()
                self.build_state.status = Build.STARTED
            else:
                err_msg = f"Can not start a new build while in state '{self.build_state.status}'"
                self.log.error(err_msg)
                context.set_code(grpc.StatusCode.FAILED_PRECONDITION)
                context.set_details(err_msg)
                return BossRpc_pb2.CommandStatus()
        elif request.command == 'stop':
            self.build_state.status = Build.STOPPED
            cmd = BeamServiceRpc_pb2.StartExposureRequest(clear_queue=True)
            BEAM_STUB.StartExposure(cmd)
        elif request.command == 'pause':
            self.build_state.status = Build.PAUSED
        else:
            raise NotImplementedError(f"Command '{request.command}' is not implemented.")

        return BossRpc_pb2.CommandStatus()


class BossStateMachine(threading.Thread):

    def __init__(self, cfg, mqtt_client, build_state):
        super().__init__()
        self.cfg = cfg
        self.mqtt = mqtt_client
        self.build_state = build_state

        self.stop_event = threading.Event()

        self.log = logging.getLogger("BossMAIN:StateMachine")

    def stop(self):
        self.stop_event.set()

    def run(self):
        self.log.info("Starting BOSS state-machine")

        # Setup the Build States
        stop_state = StopState(self.cfg, self.mqtt, None)  # We need to set pattern_state later because of circular references
        start_hv_state = StartHVState(self.cfg, self.mqtt, stop_state)
        layer_feed_state = LayerfeedState(self.cfg, self.mqtt, start_hv_state, None, stop_state)  # We need to set pattern_state later because of circular references
        complete_state = CompleteState(self.cfg, self.mqtt, stop_state)
        pattern_state = PatternState(self.cfg, self.mqtt, layer_feed_state, stop_state, complete_state)
        start_heat_state = StartHeatState(self.cfg, self.mqtt, layer_feed_state, stop_state)
        idle_state = IdleState(self.cfg, self.mqtt, start_hv_state, start_heat_state)

        # Fix circular state reference
        stop_state.idle_state = idle_state
        layer_feed_state.pattern_state = pattern_state

        # Start with IDLE state
        current_state = idle_state

        self.log.info(f"Starting state-machine with {current_state}. "
                      f"Build: {self.build_state.status}")
        current_state.enter(self.build_state)  # Lets enter the IDLE state
        while not self.stop_event.is_set():

            # Handle PAUSE state in state machine
            if self.build_state.status == Build.PAUSED:
                # TODO: Pause should be a state in the state-machine
                self.log.info(" = PAUSING build = ")
                topic = f'{current_state.topic_base}/Boss/State/Boss'
                m = MQTTMessage(topic, {
                    'BossState': 'PauseState',
                    'BuildStatus': self.build_state.status.name,
                    'BuildStatusNumber': self.build_state.status.value})
                self.mqtt.publish(m.topic, m.payload, retain=True)
                while not self.stop_event.is_set() and \
                        self.build_state.status == Build.PAUSED:
                    self.stop_event.wait(0.1)  # Don't hog the thread too often
                self.log.info(f" = Continuing build = Build: {self.build_state.status}")

            # Execute state
            new_state = current_state.run(self.build_state)

            if new_state != current_state:
                current_state.exit(self.build_state)  # Cleanup/Log etc
                new_state.enter(self.build_state)     # Setup the new state
                self.log.info(f"Transition from {current_state} -> {new_state}. "
                              f"BuildStatus: {self.build_state.status.name}")

            # Move to next state (or stay in same)
            current_state = new_state

        self.log.info('BOSS state-machine thread loop completed.')
