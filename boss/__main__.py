#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2019,2020 Freemelt AB
#
# SPDX-License-Identifier: GPL-3.0-only

"""This module is the entry point of the BOSS service"""

import argparse
import contextlib
import logging
import time
import sys

# Freemelt
from servicelib import Configurator, confighelpers

# Project
from boss.grpc_servicer import Servicer
from boss.freemeltapi import BossRpc_pb2_grpc as rpc


LOG = logging.getLogger(__name__)


def main(args):
    """Service main function"""
    LOG.info('BOSS Service started.')

    # Load service configuration
    cfg = Configurator.load_config("boss.yaml")
    confighelpers.setup_logging(
        cfg, journal_kwargs=dict(SYSLOG_IDENTIFIER=__package__))

    # Service enter/exit stack
    with contextlib.ExitStack() as stack:
        mqtt_client = \
            stack.enter_context(confighelpers.get_mqtt_client(cfg))
        servicer = \
            stack.enter_context(Servicer(cfg, mqtt_client))
        server = \
            stack.enter_context(confighelpers.get_grpc_server(cfg, rpc, servicer))

        try:
            while servicer.state_machine.is_alive():
                time.sleep(3)
        except KeyboardInterrupt:
            LOG.info('Shutdown event triggered!')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--client', action='store_true',
        help='Start minimal TK GUI gRPC client')
    args = parser.parse_args()
    if args.client:
        # Start minimal TK GUI gRPC client
        from boss import client
        client.main(args)
    else:
        try:
            main(args)
        except Exception:
            # This is added to make sure journal receives the traceback
            LOG.exception('Service failure')
            sys.exit(1)
